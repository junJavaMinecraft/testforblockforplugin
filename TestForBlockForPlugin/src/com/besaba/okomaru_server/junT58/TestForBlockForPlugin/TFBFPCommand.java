package com.besaba.okomaru_server.junT58.TestForBlockForPlugin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TFBFPCommand implements CommandExecutor{

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length <= 4){
			sender.sendMessage(ChatColor.RED + "/" + label + " <x> <y> <z> <itemid> <command>");
			return true;
		}
		if(sender instanceof Player){
			Player player = (Player) sender;
			int x;
			int y;
			int z;
			int id;
			try {
				x = Integer.parseInt(args[0]);
				y = Integer.parseInt(args[1]);
				z = Integer.parseInt(args[2]);
				id = Integer.parseInt(args[3]);
			} catch (NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invaild arguments.");
				return true;
			}
			Location blocklocation = new Location(player.getWorld(), x, y, z);
			if(blocklocation.getBlock().getTypeId() == id){
				String str = "";
				int lengt = 0;
				while(true){
					if(lengt == 0 || lengt == 1 || lengt == 2 || lengt == 3){

					}else{
						if(str.equalsIgnoreCase("")){
							str = args[lengt];
						}else{
							str = str + " " + args[lengt];
						}
					}
					lengt++;
					if(lengt == args.length){
						break;
					}
				}
				Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), str);
			}
		}else if(sender instanceof BlockCommandSender){
			BlockCommandSender blockcommandsender = (BlockCommandSender) sender;
			int x;
			int y;
			int z;
			int id;
			try {
				x = Integer.parseInt(args[0]);
				y = Integer.parseInt(args[1]);
				z = Integer.parseInt(args[2]);
				id = Integer.parseInt(args[3]);
			} catch (NumberFormatException e){
				sender.sendMessage(ChatColor.RED + "Invaild arguments.");
				return true;
			}
			Location blocklocation = new Location(blockcommandsender.getBlock().getWorld(), x, y, z);
			if(blocklocation.getBlock().getTypeId() == id){
				String str = "";
				int lengt = 0;
				while(true){
					if(lengt == 0 || lengt == 1 || lengt == 2 || lengt == 3){

					}else{
						if(str.equalsIgnoreCase("")){
							str = args[lengt];
						}else{
							str = str + " " + args[lengt];
						}
					}
					lengt++;
					if(lengt == args.length){
						break;
					}
				}
				Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), str);
			}
		}else{
			sender.sendMessage(ChatColor.RED + "Invaild Sender");
		}
		return true;
	}

}
